# Cyclic Pendulum by Simscape Multibody

## Abstract

I would like to simulate simple physics experiments by Matlab.

This repository show cyclic pendulum simulation every 1 min.

## Environment

- Matlab 2023b
- Simlink
- Simscape
- Simscape Multibody

## How to play

1. Git clone this reposity.
2. Open Matlab 2023b at this reposity folder.
3. Open `cyclic_pendulum.slx` and play the simulation.

## Result

![](fig/cyclic_pendulum.gif)

## Remarks

Overview of this model is blow.

![model](fig/model.png)
