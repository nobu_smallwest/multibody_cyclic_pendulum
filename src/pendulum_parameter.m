% This m-file is used for cyclic_pendulum.slx. 
% It is set as callback InitFcn of the model.

%% Initialise
clear
clc
close all
%% Pendulum Parameter
gravity = -9.8; % gravity[m/s2]
num = 11; % number of pendulum
positionX = linspace(-0.3, 0.3, num); % position of pendukum[m]
lengthPen = [42.22 40.45 38.78 37.22 35.74 34.38 33.07 31.84 30.64 29.54]/100; % length of pendulum[m]
radiusBall = 0.03; % radius of ball[m]

